USE [master]
GO
CREATE DATABASE [Test2]
GO
USE [Test2]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Permission](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NameEmployee] [varchar](128) NOT NULL,
	[LastNameEmployee] [varchar](128) NOT NULL,
	[PermissionTypeId] [int] NOT NULL,
	[PermissionDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [PermissionType](
	[PermissionTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
 CONSTRAINT [PK_PermissionType] PRIMARY KEY CLUSTERED 
(
	[PermissionTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [Permission] ON 
GO
INSERT [Permission] ([Id], [NameEmployee], [LastNameEmployee], [PermissionTypeId], [PermissionDate]) VALUES (1, N'Enmanuel', N'Gonzale', 1, CAST(N'2020-11-13T00:00:00.000' AS DateTime))
GO
INSERT [Permission] ([Id], [NameEmployee], [LastNameEmployee], [PermissionTypeId], [PermissionDate]) VALUES (2, N'Camilo', N'Perez', 1, CAST(N'2020-11-27T00:00:00.000' AS DateTime))
GO
INSERT [Permission] ([Id], [NameEmployee], [LastNameEmployee], [PermissionTypeId], [PermissionDate]) VALUES (3, N'Juana', N'Garcia', 2, CAST(N'2020-11-16T00:00:00.000' AS DateTime))
GO
INSERT [Permission] ([Id], [NameEmployee], [LastNameEmployee], [PermissionTypeId], [PermissionDate]) VALUES (4, N'Juan', N'Vicente', 2, CAST(N'2020-12-04T00:00:00.000' AS DateTime))
GO
INSERT [Permission] ([Id], [NameEmployee], [LastNameEmployee], [PermissionTypeId], [PermissionDate]) VALUES (10, N'Camilo', N'Perez', 1, CAST(N'2020-11-20T00:00:00.000' AS DateTime))
GO
INSERT [Permission] ([Id], [NameEmployee], [LastNameEmployee], [PermissionTypeId], [PermissionDate]) VALUES (26, N'Camilo', N'Sesto', 1, CAST(N'2020-11-19T00:00:00.000' AS DateTime))
GO
INSERT [Permission] ([Id], [NameEmployee], [LastNameEmployee], [PermissionTypeId], [PermissionDate]) VALUES (28, N'Pedro', N'Encarnacion', 1, CAST(N'2020-11-19T00:00:00.000' AS DateTime))
GO
INSERT [Permission] ([Id], [NameEmployee], [LastNameEmployee], [PermissionTypeId], [PermissionDate]) VALUES (43, N'Maria', N'Terrero', 2, CAST(N'2020-11-26T00:00:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [Permission] OFF
GO
SET IDENTITY_INSERT [PermissionType] ON 
GO
INSERT [PermissionType] ([PermissionTypeId], [Description]) VALUES (1, N'Enfermedad')
GO
INSERT [PermissionType] ([PermissionTypeId], [Description]) VALUES (2, N'Diligencia')
GO
SET IDENTITY_INSERT [PermissionType] OFF
GO
ALTER TABLE [Permission]  WITH CHECK ADD  CONSTRAINT [FK_Permission_PermissionType] FOREIGN KEY([PermissionTypeId])
REFERENCES [PermissionType] ([PermissionTypeId])
GO
ALTER TABLE [Permission] CHECK CONSTRAINT [FK_Permission_PermissionType]
GO
USE [master]
GO
ALTER DATABASE [Test2] SET  READ_WRITE 
GO
