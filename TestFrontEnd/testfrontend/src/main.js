import Vue from 'vue'
import App from './App.vue'



import 'jquery';
import 'popper.js';
import { BootstrapVue, IconsPlugin, BootstrapVueIcons  } from 'bootstrap-vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSweetalert2 from 'vue-sweetalert2';
import VueToastr2 from 'vue-toastr-2'

import 'vue-toastr-2/dist/vue-toastr-2.min.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'sweetalert2/dist/sweetalert2.min.css';
import router from './router'



window.toastr = require('toastr')

Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(IconsPlugin)
Vue.use(VueSweetalert2);
Vue.use(VueToastr2)


new Vue({
  router,

  //router,
  render: h => h(App)
}).$mount('#app')
